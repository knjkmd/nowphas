#!/usr/bin/env python3

import os
import numpy as np
import matplotlib.pyplot as plt

from nowphas_data_reader import NowphasDataReader


if __name__ == '__main__':
    data_dir = '/home/kenji/workspace/coast/nowphas/downloaded'
    filename = 'h1010.011.txt'
    filepath = os.path.join(data_dir, filename)

    data_reader = NowphasDataReader()
    data = data_reader.read_nowphas_txt(filepath)
            
    # anotehr way to create range using stndard lib
    #bins = [i*11.25 for i in range(33)]
    delta = 22.5
    half_delta = delta/2.0
    bins = np.arange(0, 361, half_delta)
    counts_by_half_delta, _ = np.histogram(data.angle, bins=bins)
    counts_by_direction = []
    
    counts_by_direction.append(counts_by_half_delta[0] + counts_by_half_delta[-1]) # Range 348.75 - 11.25
    for i in range(1, len(counts_by_half_delta)-1,2):
        counts = counts_by_half_delta[i] + counts_by_half_delta[i+1]
        counts_by_direction.append(counts)
        
        print(str(i) + "+" +str(i+1))
    
    direction_labels = [
            'N',
            'NNE',
            'NE',
            'ENE',
            'E',
            'ESE',
            'SE',
            'SSE',
            'S',
            'SSW',
            'SW',
            'WSW',
            'W',
            'WNW',
            'NW',
            'NNW',
            ]    
    
    
    x_pos = np.arange(len(counts_by_direction))
    plt.bar(x_pos, counts_by_direction,  align='center', alpha=0.5)
    plt.xticks(x_pos, direction_labels)
    plt.title('Counts by directions')
