import unittest

import datetime
import json
import os
import pandas as pd

from nowphas_data_reader import NowphasDataReader

class NowphasDataReaderTestCase(unittest.TestCase):
    def setUp(self):
        self.reader = NowphasDataReader()

    def tearDown(self):
        pass

    def test__read_nowphas_txt(self):
        data_dir = '/home/kenji/workspace/coast/nowphas/downloaded'
        filename = 'h1010.011.txt'
        filepath = os.path.join(data_dir, filename)
        data = self.reader.read_nowphas_txt(filepath)
        first_index = data.index[0].to_pydatetime()
        self.assertTrue(isinstance(data.index[0], datetime.datetime))
        self.assertTrue( (data.sig_h>99.0).sum() == 0)
        self.assertTrue( (data.sig_t>999.0).sum() == 0)
        self.assertTrue( (data.angle>9990).sum() == 0)
            
    def test__get_data(self):
        json_string = self.reader.get_data(location=None, start_time=None, end_time=None)
        data = json.loads(json_string)
        self.assertTrue(isinstance(data, dict))


    