#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 12 21:19:21 2020

@author: kenji
"""

import numpy as np
import matplotlib.pyplot as plt

class FourierTransformer():
    def __init__(self):
        pass
    
    def DFT(self, x):
        """
        Compute the discrete Fourier Transform of the 1D array x
        :param x: (array)
        """
    
        N = x.size
        n = np.arange(N)
        k = n.reshape((N, 1))
        e = np.exp(-2j * np.pi * k * n / N)
        return np.dot(e, x)        


if __name__=='__main__':
    ft = FourierTransformer()
    
    
    t = np.linspace(0, 10.0, 500)
    s = np.sin(45 * 2* np.pi * t) #+ 0.5 * np.sin(90 * 2 * np.pi * t)

    
    plt.ylabel("Amplitude")
    plt.xlabel("Time [s]")
    plt.plot(t, s)
    plt.show()

#%%
    dft_res = ft.DFT(s)
    for i in range(2):
        print("Value at index {}:\t{}".format(i, dft_res[i + 1]), "\nValue at index {}:\t{}".format(dft_res.size -1 - i, dft_res[-1 - i]))

#%%
    fft_res = fft = np.fft.fft(s)
    for i in range(2):
        print("Value at index {}:\t{}".format(i, fft_res[i + 1]), "\nValue at index {}:\t{}".format(fft_res.size -1 - i, fft_res[-1 - i]))

#%%
    fft = np.fft.fft(s)
    T = t[1] - t[0]  # sampling interval 
    N = s.size
    
    # 1/T = frequency
    f = np.linspace(0, 1 / T, N)
    
    plt.ylabel("Amplitude")
    plt.xlabel("Frequency [Hz]")
    plt.bar(f[:N // 2], np.abs(fft)[:N // 2] * 1 / N, width=1.5)  # 1 / N is a normalization factor
    plt.show()
