#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import numpy as np
import os
import pandas as pd
import re

# TODO: Can this be a class or static method?
def _create_colspecs(fortran_format):
    colspecs = []
    index = 0
    for field in fortran_format.split(','):
        multiplier, width = re.findall(r'(\d+)?[FI](\d+)', field)[0]
        try:
            multiplier = int(multiplier)
        except ValueError:
            multiplier = 1

        for i in range(multiplier):
            colspecs.append( (index, index + int(width)) )
            index += int(width)            
        
    return colspecs


class NowphasDataReader():
    fortran_format = 'I4,3I2,I6,I6,F6.2,F6.1,F6.2,F6.1,F6.2,F6.1,F6.2,F6.1,I6'
    columns = [
        'year', 
        'month', 
        'day', 
        'hour', 
        'flag', 
        'nwave', 
        'ave_h', 
        'ave_t', 
        'sig_h', 
        'sig_t', 
        'tenth_h', 
        'tenth_t', 
        'max_h', 
        'max_t', 
        'angle']
    colspecs = _create_colspecs(fortran_format)
    
    def __init__(self):
        pass
    
    def read_nowphas_txt(self, filepath):
        data = pd.read_fwf(filepath, skiprows=1, colspecs=self.colspecs, header=None)
        data.columns = self.columns
        self._set_index_to_datetime(data)
        self._replace_value_for_missing(data)
        return data

    def _set_index_to_datetime(self, data):
        data['datetime'] = pd.to_datetime({'year':data.year, 'month':data.month, 'day':data.day, 'hour':data.hour})
        data.set_index('datetime',inplace=True)
        
    def _replace_value_for_missing(self, data):
        for column in ['ave_h', 'sig_h', 'tenth_h', 'max_h']:
            data.loc[data[column]>99.0, column] = np.nan
        for column in ['ave_t', 'sig_t', 'tenth_t', 'max_t']:
            data.loc[data[column]>999.0, column] = np.nan
        data.loc[data['angle']>9990, 'angle'] = np.nan
        
    def get_data(self, location=None, start_time=None, end_time=None):
        # TODO: Get data from database properly
        data_dir = '/home/kenji/workspace/coast/nowphas/downloaded'
        filename = 'h1010.011.txt'
        filepath = os.path.join(data_dir, filename)
        data = self.read_nowphas_txt(filepath)
        _dict = {
                'location': 'Akita',
                'start_time': data.index[0].value,
                'end_time': data.index[-1].value,
                'data': data.sig_h.tolist()
                }
        
        return json.dumps(_dict)

        
        
    
    